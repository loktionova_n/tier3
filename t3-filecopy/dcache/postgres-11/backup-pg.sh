#!/bin/bash 

 mail_to="cms-tier3@lists.psi.ch"
 backup_dir="/pg-backup"
 # backup_dir="/var/lib/pgsql/11/backups"
 backup_file="pg_`date +\%Y-\%m-\%d`.sql.gz"
 log_file=" /var/log/PG-backup-`date +\%Y-\%m-\%d`"

#Logging all the output to a LogFile:
exec 1> $log_file 2>&1

 function send_warning_and_exit()
 {

 cat <<EOF | /usr/sbin/sendmail -t
To:  $mail_to
Subject: PG backup TROUBLE  on  `hostname` 

  echo $1
 `date`

EOF
   echo "***** PG backup FAILURE ***** "
   `date`
   exit 1
}

if [ ! -d $backup_dir ]
  then
  send_warning_and_exit "$backup_dir dosen't exist"
fi

if [ -s $backup_dir/$backup_file ]
  then
  send_warning_and_exit "$backup_file  exists"
fi


echo "start PGDB dump"  `date` 

pg_dumpall -U postgres|gzip >  $backup_dir/$backup_file  

echo "end PGDB dump"  `date` 

# cleaning old:
find $backup_dir -type f -name '*.gz' -mtime +2  -exec rm {} \;
