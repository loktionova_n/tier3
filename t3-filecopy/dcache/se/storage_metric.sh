#!/bin/bash

for metric in total free
do
    wget http://t3dcachedb03:2288/info/poolgroups/cms/space/${metric}?format=json -q -O /tmp/metric.out
    grep ${metric} /tmp/metric.out > /dev/null
    if [ $? == 0 ]
    then
        value=`grep ${metric} /tmp/metric.out | awk '{print $2/1024^4}' | awk -F "." '{print $1}'`
        /usr/bin/gmetric -t uint16 -n storage_volume_${metric} -u TB -v ${value}
    fi
    rm -rf /tmp/metric.out
done
