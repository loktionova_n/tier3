if [ "X${GLITE_ENV_SET+X}" = "X" ]; then
. /etc/profile.d/grid-env-funcs.sh
if [ "x${GLITE_UI_ARCH:-$1}" = "x32BIT" ]; then arch_dir=lib; else arch_dir=lib64; fi
gridpath_prepend     "PATH" "/bin"
gridpath_prepend     "MANPATH" "/opt/glite/share/man"
gridenv_setind      "X509_USER_PROXY" "$HOME/.x509up_u$(id -u)"
gridenv_set         "X509_CERT_DIR" "/etc/grid-security/certificates"
gridenv_set         "X509_VOMS_DIR" "/etc/grid-security/vomsdir"
gridenv_set         "VOMS_USERCONF" "/etc/vomses"
gridenv_set         "VO_CMS_SW_DIR" "/cvmfs/cms.cern.ch"
gridenv_set         "VO_CMS_DEFAULT_SE" "t3se01.psi.ch"
gridenv_set         "SRM_PATH" "/usr/share/srm"
gridenv_set         "MYPROXY_SERVER" "myproxy.cern.ch"
gridenv_set         "LCG_LOCATION" "/usr"
gridenv_set         "LCG_GFAL_INFOSYS" "lcg-bdii.cern.ch:2170"
gridenv_set         "BDII_LIST" "lcg-bdii.cern.ch:2170"
gridenv_set         "GT_PROXY_MODE" "old"
gridenv_set         "GRID_ENV_LOCATION" "/etc/profile.d"
gridenv_set         "GLOBUS_TCP_PORT_RANGE" "20000,25000"
gridenv_set         "GLITE_SD_SERVICES_XML" "/usr/etc/services.xml"
gridenv_set         "GLITE_SD_PLUGIN" "file,bdii"
gridenv_set         "GLITE_LOCATION_VAR" "/var"
gridenv_set         "GLITE_LOCATION" "/usr"
gridenv_set         "DPNS_HOST" "my-dpm.psi.ch"
gridenv_set         "DPM_HOST" "my-dpm.psi.ch"
. /etc/profile.d/clean-grid-env-funcs.sh
fi

