#!/bin/bash

#CPU
for part in quick wn eos
do
    /root/cpu-R-vs-PD-graph.sh  $part >/dev/null  2>&1
   /root/cpu-R-graph.sh         $part >/dev/null  2>&1
done

#GPU
for part in gpu qgpu
do
   /root/gpu-R-vs-PD-graph.sh   $part >/dev/null  2>&1
done
