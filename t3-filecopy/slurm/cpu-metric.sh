#!/bin/bash

cpu_t3_overall=`sinfo -o "%C"|sed "1d"| awk -F/ '{print $4}'`
if [ $? == 0 ]
    then
       /usr/bin/gmetric -t uint16  -n cpu_t3_overall -u N -v ${cpu_t3_overall}
fi

for partition in `sinfo -o " %P"| sed "1d"|sed 's/*//' `
do
    cpu_Total=`sinfo -o "%C  %P"|sed "1d" |grep -w ${partition}|awk  '{print $1}'|awk -F"/" '{print $4}'`
    if [ $? == 0 ]
    then
        /usr/bin/gmetric -t uint16 -n ${partition}_cpu_Total -u N -v ${cpu_Total}
    fi
    
    cpu_R=`squeue -r -o "%P %C %t"|grep ${partition}|grep  " R$" |awk 'BEGIN{a=0} {a+=$2} END{print a}'`
    if [ $? == 0 ]
    then
        /usr/bin/gmetric -t uint16 -n ${partition}_cpu_R -u N -v ${cpu_R}
    fi

    cpu_PD=`squeue -r -o "%P %C %t"|grep ${partition}|grep  " PD$" |awk 'BEGIN{a=0} {a+=$2} END{print a}'`
    if [ $? == 0 ]
    then
        /usr/bin/gmetric -t uint16 -n ${partition}_cpu_PD -u N -v ${cpu_PD}
    fi
done
