#!/bin/sh
#
# This script will kill any user processes on a node when the last
# SLURM job there ends. For example, if a user directly logs into
# an allocated node SLURM will not kill that process without this
# script being executed as an epilog.
#
# SLURM_BIN can be used for testing with private version of SLURM
#SLURM_BIN="/usr/bin/"
#
if [ x$SLURM_UID = "x" ] ; then 
	exit 0
fi
if [ x$SLURM_JOB_ID = "x" ] ; then 
        exit 0
fi

#
# Don't try to kill user root or system daemon jobs
#
if [ $SLURM_UID -lt 100 ] ; then
	exit 0
fi

# Delete cgroups for this job and log deleted files/directories in /tmp
#   - Temporary solution for solving a bug when job does not finisih by his own (i.e. CANCELLED), where cgroups allocation is not freed up
#   - This must be done before checking if other jobs are running and exit.
#   - In addition, once user has no more jobs running, some other deletion actions must be performed (see below)
# find /sys/fs/cgroup/*/slurm/uid_$SLURM_JOB_UID/job_$SLURM_JOB_ID -print -delete >> /tmp/cgroups_deleted.uid_$SLURM_JOB_UID 2> /dev/null

# If user has other jobs running, do not proceed and exit
# This will avoid to delete other files from other processes
# and also 
job_list=`${SLURM_BIN}squeue --noheader --format=%A --user=$SLURM_UID --node=localhost`
for job_id in $job_list
do
	if [ $job_id -ne $SLURM_JOB_ID ] ; then
		exit 0
	fi
done

#

# Drop clean caches (OpenHPC)
echo 3 > /proc/sys/vm/drop_caches

#
# No other SLURM jobs, purge all remaining processes of this user
#
pkill -KILL -U $SLURM_UID

# Remove any files the user created in /tmp
#   - Remove first in a recursive mode the directory at level 1
#   - Then remove any other files in the level 1
if [ -d /scratch ]
then 
  /bin/find /scratch -maxdepth 1 -user ${SLURM_JOB_USER} -type 'd' -print0 | /usr/bin/xargs -0 -r /bin/rm -rf
  /bin/find /scratch -maxdepth 1 -user ${SLURM_JOB_USER} -type 'f' -print0 | /usr/bin/xargs -0 -r /bin/rm -f
fi

# Delete cgroups for this user and log deleted files/directories in /tmp
#   - Temporary solution for solving a bug when job does not finisih by his own (i.e. CANCELLED), where cgroups allocation is not freed up
#   - In addition to previous cgroup deletion for jobid, once user has no more jobs running, delete entries for the user.
# find /sys/fs/cgroup/*/slurm/uid_$SLURM_JOB_UID -print -delete >> /tmp/cgroups_deleted.uid_$SLURM_JOB_UID 2> /dev/null

exit 0
