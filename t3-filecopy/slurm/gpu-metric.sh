#!/bin/bash

gpu_Total=16
qgpu_Total=16

for partition in gpu qgpu
do

    /usr/bin/gmetric -t uint16 -n ${partition}_Total -u N -v 16


    squeue | grep -w  ${partition} > /tmp/jobs.tmp
    grep " R " /tmp/jobs.tmp > /dev/null
    if [ $? = 0 ]
    then 
        gpu_R=`for i in $(cat /tmp/jobs.tmp | grep " R " | awk '{print $1}'); \
                 do scontrol show job $i | grep "gres/gpu" | awk -F "gres/gpu=" '{print $2}'; \
               done | awk 'BEGIN{a=0} {a+=$1} END{print a}'`
    else
        gpu_R=0
    fi
    /usr/bin/gmetric -t uint16 -n ${partition}_R -u N -v ${gpu_R}
    
    grep " PD " /tmp/jobs.tmp > /dev/null
    if [ $? = 0 ]
    then  
        gpu_PD=`for i in $(cat /tmp/jobs.tmp | grep " PD " | awk '{print $1}'); \
                  do scontrol show job $i | grep "gres/gpu" | awk -F "gres/gpu=" '{print $2}'; \
                done | awk 'BEGIN{a=0} {a+=$1} END{print a}'`
    else
        gpu_PD=0
    fi
    /usr/bin/gmetric -t uint16 -n ${partition}_PD -u N -v ${gpu_PD}


done


